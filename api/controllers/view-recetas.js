module.exports = {


  friendlyName: 'View recetas',


  description: 'Display "Recetas" page.',


  exits: {

    success: {
      viewTemplatePath: 'recetas/recetas'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
