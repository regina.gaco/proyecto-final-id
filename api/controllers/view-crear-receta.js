module.exports = {


  friendlyName: 'View crear receta',


  description: 'Display "Crear receta" page.',


  exits: {

    success: {
      viewTemplatePath: 'recetas/crear-receta'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
