module.exports = {


  friendlyName: 'View crear usuario',


  description: 'Display "Crear usuario" page.',


  exits: {

    success: {
      viewTemplatePath: 'usuarios/crear-usuario'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
