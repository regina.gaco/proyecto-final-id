module.exports = {


  friendlyName: 'View crear usuario',


  description: 'Display "Crear usuario" page.',


  exits: {

    success: {
      viewTemplatePath: 'usuarios/usuarios'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
