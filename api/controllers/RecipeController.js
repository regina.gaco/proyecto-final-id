/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  //Muestra todo los usarios de /recetas
  recetas:function(req, res){
        
    var valuesToSet = {};
    if(req.param('id'))
    _.extend(valuesToSet, {id:req.param('id')});
    if(req.param('nombre'))
    _.extend(valuesToSet, {nombre:req.param('nombre')});
    if(req.param('pasos'))
    _.extend(valuesToSet, {pasos:req.param('pasos')});
    if(req.param('ingredientes'))
    _.extend(valuesToSet, {ingredientes:req.param('ingredientes')});
    if(req.param('allergy1'))
    _.extend(valuesToSet, {allergy1:req.param('allergy1')});
    if(req.param('allergy2'))
    _.extend(valuesToSet, {allergy2:req.param('allergy2')});
    if(req.param('allergy3'))
    _.extend(valuesToSet, {allergy3:req.param('allergy3')});
    if(req.param('allergy4'))
    _.extend(valuesToSet, {allergy4:req.param('allergy4')});
    if(req.param('allergy5'))
    _.extend(valuesToSet, {allergy5:req.param('allergy5')});
    if(req.param('categoria'))
    _.extend(valuesToSet, {categoria:req.param('categoria')});
    //Find() regresa todo los usarios del schema User
    User.find(valuesToSet).exec(function(err, receta){
    if(err){
      res.send(500, {error: 'Database Error'});
    }
    res.view('recetas/recetas', {receta:receta});
   });
 },

   //El acccion en si de crear un nuevo receta
   crearReceta:function(req,res){
   //Guarda la informacion que regresa la pagina /recetas/crearReceta
    var nombre =  req.body.nombre;
    var pasos  =  req.body.pasos;
    var ingredientes =  req.body.ingredientes;
    var allergy1 =  req.body.allergy1;
    var allergy2 =  req.body.allergy2;
    var allergy3 =  req.body.allergy3;
    var allergy4 =  req.body.allergy4;
    var allergy5 =  req.body.allergy5;
    var categoria =  req.body.categoria;
    //Crea el nuevo usario en la schema Recipe
    Recipe.create({nombre:nombre, pasos:pasos, ingredientes:ingredientes,
      allergy1:allergy1, allergy2:allergy2, allergy3:allergy3,
      allergy4:allergy4, allergy5:allergy5,categoria:categoria}).exec(function(err){
        if(err){
        //Si hay un error, avisa
          return res.status(500).send({error: 'Database Error'});
        }
        
        //Regresa a la pagina de usuarios
        return res.redirect('/recetas');
        });
   },

   //El accion de eliminar un usario, de la vista receta/ver-receta
   eliminarReceta: function(req,res){
   //Guarda el id del usario, el cual se manda a la pagina /user/delete?id=id
   //req.params.id le que esta al final del path =id
    var id =  req.params('id');
    //Elimina del base de datos 1 receta con el mismo id
    //destroy({id:id}) tambien funciona pero para todos con el mismo id, pero como es unico no importa en este
     Recipe.destroyOne({id:id}).exec(function(err){
        if(err){
            //Si 'res.send(500, {error: 'Database Error'});' no funciona, debe de redireccionar a una pagina de error
        res.send(500, {error: 'Database Error'});
        }
        
        //regresa a /recetas
        return res.redirect('/recetas');
     });
   },

   //El accion de ir a la pagina para editar
   editarReceta: function(req,res){
    var id =  req.param('id');
     Recipe.findOne({id:id}).exec(function(err,receta){
        if(err){
        //Si hay un error, avisa
          res.send(500, {error: 'Database Error'});
        }
        
        return res.view('recetas/editar-receta',{receta:receta});
     });
   },

      //El accion de ir a la pagina para editar
   verReceta: function(req,res){
    var id =  req.param('id');
     User.findOne({id:id}).exec(function(err,receta){
        if(err){
        //Si hay un error, avisa
        res.send(500, {error: 'Database Error'});
        }

        return res.view('recetas/ver-receta',{receta:receta});
     });
   },

   //Usar la informacion de la pagina de editar, para actualizar el usario
   update: function(req,res){
   
   //Guarda la informacion que regresa la pagina /user/edit
    var nombre =  req.body.nombre;
    var contrasena  =  req.body.contrasena;
    var correo =  req.body.correo;
    var peso =  req.body.peso;
    var estatura =  req.body.estatura;

    //Guardar que usario Actualizar
    var id =  req.params.id;

    //Actualizar el usario en la schema User
    Recipe.update({id:id }, {nombre:nombre, contrasena:contrasena,
      correo:correo, peso:peso, estatura:estatura}).exec(function(err){
        if(err){
        //Si hay un error, avisa
          res.send(500, {error: 'Database Error'});
        }
        
        //Regresa a la pagina de usuarios
        return res.redirect('/user/users');
        });
   }
};

