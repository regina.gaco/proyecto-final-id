/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  //Muestra todo los usarios de /usuarios
  usuarios:function(req, res){
        
    var valuesToSet = {};
    if(req.param('id'))
    _.extend(valuesToSet, {id:req.param('id')});
    if(req.param('nombre'))
    _.extend(valuesToSet, {nombre:req.param('nombre')});
    if(req.param('peso'))
    _.extend(valuesToSet, {peso:req.param('peso')});
    if(req.param('estatura'))
    _.extend(valuesToSet, {estatura:req.param('estatura')});
    if(req.param('allergy1'))
    _.extend(valuesToSet, {allergy1:req.param('allergy1')});
    if(req.param('allergy2'))
    _.extend(valuesToSet, {allergy2:req.param('allergy2')});
    if(req.param('allergy3'))
    _.extend(valuesToSet, {allergy3:req.param('allergy3')});
    if(req.param('allergy4'))
    _.extend(valuesToSet, {allergy4:req.param('allergy4')});
    if(req.param('allergy5'))
    _.extend(valuesToSet, {allergy5:req.param('allergy5')});
    //Find() regresa todo los usarios del schema User
    User.find(valuesToSet).exec(function(err, user){
    if(err){
      res.send(500, {error: 'Database Error'});
    }
    res.view('usuarios/usuarios', {user:user});
   });
 },
  
   buscar:function(req,res){
   //Guarda la informacion que regresa la pagina /usuario/crearUsuario
    var nombre =  req.body.nombre;
    var peso =  req.body.peso;
    var estatura =  req.body.estatura;
    var allergy1 =  req.body.allergy1;
    var allergy2 =  req.body.allergy2;
    var allergy3 =  req.body.allergy3;
    var allergy4 =  req.body.allergy4;
    var allergy5 =  req.body.allergy5;

    var search = "?";
    if(nombre != null)
        search += "nombre=" + nombre + "&"; 
    if(peso != null)
        search += "peso=" + peso + "&"; 
    if(peso != null)
        search += "estatura=" + estatura + "&"; 
    if(allergy1 != null)
        search += "allergy1=" + allergy1 + "&"; 
    if(allergy2 != null)
        search += "allergy2=" + allergy2 + "&"; 
    if(allergy3 != null)
        search += "allergy3=" + allergy3 + "&"; 
    if(allergy4 != null)
        search += "allergy4=" + allergy4 + "&"; 
    if(allergy5 != null)
        search += "allergy5=" + allergy5 + "&"; 
        //Regresa a la pagina de usuarios

        search = '/usuarios/' +search;
     return res.redirect(search);
   },

   //El acccion en si de crear un nuevo usario
   crearUsuario:function(req,res){
   //Guarda la informacion que regresa la pagina /usuario/crearUsuario
    var nombre =  req.body.nombre;
    var contrasena  =  req.body.contrasena;
    var correo =  req.body.correo;
    var peso =  req.body.peso;
    var estatura =  req.body.estatura;
    var allergy1 =  req.body.allergy1;
    var allergy2 =  req.body.allergy2;
    var allergy3 =  req.body.allergy3;
    var allergy4 =  req.body.allergy4;
    var allergy5 =  req.body.allergy5;
    //Crea el nuevo usario en la schema User
    User.create({nombre:nombre, contrasena:contrasena,
      correo:correo, peso:peso, estatura:estatura,
       allergy1:allergy1, allergy2:allergy2, allergy3:allergy3,
      allergy4:allergy4, allergy5:allergy5}).exec(function(err){
        if(err){
        //Si hay un error, avisa
          res.status(500).send({error: 'Database Error'});
        }
        
        //Regresa a la pagina de usuarios
        return res.redirect('/usuarios');
        });
   },

   //El accion de eliminar un usario, de la vista usuario/ver-usuario
   eliminarUsuario: function(req,res){
   //Guarda el id del usario, el cual se manda a la pagina /user/delete/user.id
   //req.params.id le que esta al final del path /id
    var id =  req.param('id');
    //Elimina del base de datos 1 usario con el mismo id
    //destroy({id:id}) tambien funciona pero para todos con el mismo id, pero como es unico no importa en este
     User.destroyOne({id:id}).exec(function(err){
        if(err){
            //Si 'res.send(500, {error: 'Database Error'});' no funciona, debe de redireccionar a una pagina de error
        res.send(500, {error: 'Database Error'});
        }
        
        //Si 'return res.redirect('/user/users')';' no funciona, pero 'back' debe de regresar a /user/users
        return res.redirect('/recetas');
     });
   },

   //El accion de ir a la pagina para editar
   editarUsuario: function(req,res){
    var id =  req.param('id');
     User.findOne({id:id}).exec(function(err,user){
        if(err){
        //Si hay un error, avisa
        res.send(500, {error: 'Database Error'});
        }

        return res.view('usuarios/editar-usuario',{user:user});
     });
   },

      //El accion de ir a la pagina para editar
   verUsuario: function(req,res){
    var id =  req.param('id');
     User.findOne({id:id}).exec(function(err,user){
        if(err){
        //Si hay un error, avisa
        res.send(500, {error: 'Database Error'});
        }

        return res.view('usuarios/ver-usuario',{user:user});
     });
   },

   //Usar la informacion de la pagina de editar, para actualizar el usario
   updateUsuario: function(req,res){

    //Guardar que usario Actualizar
    var id =   req.param('id');
   
   //Guarda la informacion que regresa la pagina /user/edit
    var nombre =  req.body.nombre;
    var contrasena  =  req.body.contrasena;
    var correo =  req.body.correo;
    var peso =  req.body.peso;
    var estatura =  req.body.estatura;
    var allergy1 =  req.body.allergy1;
    var allergy2 =  req.body.allergy2;
    var allergy3 =  req.body.allergy3;
    var allergy4 =  req.body.allergy4;
    var allergy5 =  req.body.allergy5;

    //Actualizar el usario en la schema User
    User.update({id:id }, {nombre:nombre, contrasena:contrasena,
      correo:correo, peso:peso, estatura:estatura,
       allergy1:allergy1, allergy2:allergy2, allergy3:allergy3,
      allergy4:allergy4, allergy5:allergy5}).exec(function(err){
        if(err){
        //Si hay un error, avisa
          res.send(500, {error: 'Database Error'});
        }
        
        //Regresa a la pagina de usuarios
        return res.redirect('/usuarios');
        });
   }
};

