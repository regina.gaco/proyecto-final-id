module.exports = {


  friendlyName: 'View editar usuario',


  description: 'Display "Editar usuario" page.',


  exits: {

    success: {
      viewTemplatePath: 'usuarios/editar-usuario'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
