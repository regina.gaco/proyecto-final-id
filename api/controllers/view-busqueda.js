module.exports = {


  friendlyName: 'View busqueda',


  description: 'Display "Busqueda" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/recetas/busqueda'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
