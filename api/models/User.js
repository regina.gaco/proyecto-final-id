/**
 * User.js
 *
 * A user who can log in to this application.
 */

module.exports = {

    //Test: http://localhost:1337/user/Create?fullName=Jean&emailAddress=i@i.com&password=123&peso=150&estatura=150
  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    correo: {
      type: 'string',
      required: true,
      unique: true,
      isEmail: true,
      maxLength: 200,
      example: 'mary.sue@example.com'
    },
    
    contrasena: {
      type: 'string',
      required: true,
      description: 'Securely hashed representation of the user\'s login password.',
      protect: true,
      example: '2$28a8eabna301089103-13948134nad'
    },

    nombre: {
      type: 'string',
      required: true,
      description: 'Full representation of the user\'s name.',
      maxLength: 120,
      example: 'Mary Sue van der McHenst'
    },
    //Vegetariano
    allergy1:{
     type: 'boolean'
  	},
      //Diabetico
      allergy2:{
       type: 'boolean'
  	},
      //Intolerante a la Lactosa
      allergy3:{
       type: 'boolean'
  	},
      //No consume mariscos
      allergy4:{
       type: 'boolean'
    },
      //Alergia al maní
      allergy5:{
       type: 'boolean'
    },

    peso:{
    type: 'number'

   },

   estatura:{
    type:'number'

   }

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝
    // n/a

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    // n/a

  },


};
