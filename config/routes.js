/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },

  'GET /crear-cuenta':             { action: 'view-crear-usuario' },
  'GET /login':                    { action: 'view-login' },
  'GET /busqueda':                 { action: 'view-busqueda' },
  'GET /welcome/:unused?':         { action: 'dashboard/view-welcome' },

  'GET /faq':                      { action:   'view-faq' },
  'GET /legal/terms':              { action:   'legal/view-terms' },
  'GET /legal/privacy':            { action:   'legal/view-privacy' },
  'GET /contact':                  { action:   'view-contact' },
  'GET /usuarios':              { 
    controller: 'UserController',
    action: 'usuarios' 
    },
  'POST /usuario/crearUsuario': { 
    controller: 'UserController',
    action: 'crearUsuario' 
    },
  'GET /usuario/editarUsuario': { 
    controller: 'UserController',
    action: 'editarUsuario' 
    },
    'PUT /usuario/updateUsuario': { 
    controller: 'UserController',
    action: 'updateUsuario' 
    },
  'PUT /usuario/eliminarUsuario': { 
    controller: 'UserController',
    action: 'eliminarUsuario' 
    },
  'GET /usuario/buscar': { 
    controller: 'UserController',
    action: 'buscar' 
    },
  'GET /usuario/verUsuario': { 
    controller: 'UserController',
    action: 'verUsuario' 
    },
    

  'GET /crear-receta':             { action: 'view-crear-receta' },
  'GET /recetas':             { 
    controller: 'RecipeController',
    action: 'recetas' 
    },
  'POST /recetas/crearReceta':             { 
    controller: 'RecipeController',
    action: 'crearReceta' 
    },
  'GET /receta/editarReceta': { 
    controller: 'RecipeController',
    action: 'editarReceta' 
    },
  'PUT /receta/eliminarReceta': { 
    controller: 'RecipeController',
    action: 'eliminarReceta' 
    },
  'GET /receta/verReceta': { 
    controller: 'RecipeController',
    action: 'verReceta' 
    },


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
